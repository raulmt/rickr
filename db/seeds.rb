# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(:first_name => 'Rodrigo', :last_name => 'Cuevas', :username => 'rcuevas', :password => '123456', :admin => true)

profilePhoto = Photo.create(:name => 'Foto de perfil', :path => 'perfil.jpg', :description => 'La primera foto de perfil', :user => user)

firstAlbum = Album.create(:name => 'Primer album', :description => 'El primer album de fotos de Rickr', :user => user, :photos => [profilePhoto])

coverPhoto = Photo.create(:name => 'Portada', :description => 'Foto de la portada', :path => 'cover.jpg', :user => user)

firstAlbum.albums_photos.create(:cover => true, :photo => coverPhoto)