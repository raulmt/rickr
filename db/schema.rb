# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121120132953) do

  create_table "albums", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "albums", ["user_id"], :name => "index_albums_on_user_id"

  create_table "albums_photos", :force => true do |t|
    t.integer "album_id", :null => false
    t.integer "photo_id", :null => false
    t.boolean "cover"
  end

  add_index "albums_photos", ["album_id", "photo_id"], :name => "index_albums_photos_on_album_id_and_photo_id", :unique => true

  create_table "photos", :force => true do |t|
    t.string   "path"
    t.string   "name"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "tags"
    t.boolean  "published",   :default => true
  end

  add_index "photos", ["user_id"], :name => "index_photos_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "password_digest"
    t.boolean  "admin"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "email"
    t.string   "twitter_user"
    t.string   "twitter_token"
    t.string   "twitter_secret"
  end

end
