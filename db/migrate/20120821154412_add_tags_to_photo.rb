class AddTagsToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :tags, :string
    add_column :photos, :published, :boolean, :default => true
  end
end
