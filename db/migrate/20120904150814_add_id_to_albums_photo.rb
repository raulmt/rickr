class AddIdToAlbumsPhoto < ActiveRecord::Migration
  def change
    add_column :albums_photos, :id, :primary_key
  end
end
