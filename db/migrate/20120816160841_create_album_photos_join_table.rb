class CreateAlbumPhotosJoinTable < ActiveRecord::Migration

  def change
    create_table :albums_photos, :id => false do |t|
      t.references :album, :null => false
      t.references :photo, :null => false
    end
    add_index :albums_photos, [:album_id, :photo_id], :unique => true
  end

end
