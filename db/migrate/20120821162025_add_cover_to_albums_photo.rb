class AddCoverToAlbumsPhoto < ActiveRecord::Migration
  def change
    add_column :albums_photos, :cover, :boolean
  end
end
