require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "the truth" do
     assert true
   end
  
  test "complete name" do
    u = users(:raulmt)
    assert u.complete_name == u.first_name + ' ' + u.last_name, "No sabe crear un complete name, ha! ha!"
  end
  
  test "user without unique username should not be created" do
    u = User.new(:first_name => "Raul", :last_name => "Montes", :admin => false, :username => "raulmt", :password_digest => "lalala")
    assert !u.save
  end
  
end
