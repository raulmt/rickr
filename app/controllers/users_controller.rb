class UsersController < ApplicationController

  def user_timeline
    @access_token = OAuth::AccessToken.new(UsersController.consumer, current_user.twitter_token, current_user.twitter_secret)
    @response = UsersController.consumer.request(:get, "/1.1/statuses/user_timeline.json", @access_token, { :scheme => :query_string })
    @tweets = JSON::load(@response.body)
  end


  def activate_twitter
    @request_token = UsersController.consumer.get_request_token(:oauth_callback => "http://rickr.com:3000/users/twitter_callback")
    session[:request_token] = @request_token.token
    session[:request_token_secret] = @request_token.secret
    
    redirect_to @request_token.authorize_url
  end
  
  def twitter_callback
    @request_token = OAuth::RequestToken.new(UsersController.consumer, session[:request_token], session[:request_token_secret])
    @access_token = @request_token.get_access_token
    @response = UsersController.consumer.request(:get, '/1.1/account/verify_credentials.json', @access_token, { :scheme => :query_string })
    case @response
    when Net::HTTPSuccess
      user_info = JSON.parse(@response.body)
      unless user_info['screen_name']
        flash[:notice] = "Authentication failed"
        redirect_to :action => :index
        return
      end
      
      current_user.twitter_user = user_info['screen_name']
      current_user.twitter_token = @access_token.token
      current_user.twitter_secret = @access_token.secret
      current_user.save!
      flash[:notice] = "Twitter activated!"
      redirect_to(current_user)
        
    else
      flash[:notice] = "Authentication failed"
      redirect_to :action => :index
      return
    end
  end
  
  def self.consumer
    OAuth::Consumer.new("1dUnFX5lQYT8CIR5QYAfA", "DQZeP7uyPwXVTdBeoB4VyUFiY4AGILrAYpOgE0Ed7k", { :site=>"https://api.twitter.com" })    
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
      format.xml { render xml: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    current_user
  end
  

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    respond_to do |format|
      if @user.save
        format.html {
          redirect_to @user, notice: 'User was successfully created.'
        }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
end
