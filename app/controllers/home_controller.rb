class HomeController < ApplicationController
  def index
    @user = User.find(session[:user_id]) if session[:user_id]
  end

  def login
    if u = User.find_by_username(params[:user][:username]).try(:authenticate, params[:user][:password])
      session[:user_id] = u.id
      flash[:notice] = "Bienvenido!, #{u.first_name}"
    else
      flash[:notice] = "Datos de ingreso incorrectos"
    end
    
    redirect_to :action => :index
  end

  def logout
    reset_session
    flash[:notice] = "Te deslogueaste, vuelve pronto! Si no..."
    redirect_to :action => :index
  end
  
end
