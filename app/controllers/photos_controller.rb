class PhotosController < ApplicationController

  layout 'clean_layout', :only => [:index]

  def show_photos_of_user
    @photos = Photo.where(:user_id => params[:id]).paginate(:page => params[:page], :per_page => 3)
    # para demorar mas el request ajax
    sleep 3
    respond_to do |format|
      format.html {
        if request.xhr?
          render :index, :layout => false
        else
          render :index
        end
      }
      format.js {
        @greeting = "Un saludo desde el controlador"
      }
    end
    
  end
  
  def show_photos_of_album
    @photos = Photo.joins(:albums_photos).where('albums_photos.album_id = ?', 1)
    render :index
  end
  
  # GET /photos
  # GET /photos.json
  def index
    @photos = Photo.paginate(:page => params[:page], :per_page => 3)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @photos }
    end
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    @photo = Photo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @photo }
    end
  end

  # GET /photos/new
  # GET /photos/new.json
  def new
    @photo = Photo.new
    @photo.albums_photos.build
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @photo }
    end
  end

  # GET /photos/1/edit
  def edit
    @photo = Photo.find(params[:id])
    @photo.albums_photos.build
  end

  # POST /photos
  # POST /photos.json
  def create
    Photo.process_file(params[:photo])
    @photo = Photo.new(params[:photo])
    
    respond_to do |format|
      if @photo.save
        format.html { redirect_to @photo, notice: 'Photo was successfully created.' }
        format.json { render json: @photo, status: :created, location: @photo }
      else
        format.html { render action: "new" }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /photos/1
  # PUT /photos/1.json
  def update
    @photo = Photo.find(params[:id])
    Photo.process_file(params[:photo])
    respond_to do |format|
      if @photo.update_attributes(params[:photo])
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    respond_to do |format|
      format.html { redirect_to photos_url }
      format.json { head :no_content }
    end
  end
end
