class UserMailer < ActionMailer::Base
  default from: "from@example.com"
  
  def spam
    @user = User.first
    @url = 'http://lmgtfy.com'
    attachments['user.png'] = File.read(Rails.root.join('app/assets/images/user.png'))
    mail(:to => 'raulmt@gmail.com', :subject => 'Ejemplo de correo spam')
    
    #Para enviar este correo desde un controller, model o la consola: UserMailer.spam.deliver
  end
  
end
