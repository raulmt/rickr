class Album < ActiveRecord::Base
  belongs_to :user
  has_many :photos, :through => :albums_photos
  has_many :albums_photos
  attr_accessible :description, :name, :user, :photos, :user_id
end