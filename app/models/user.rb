class User < ActiveRecord::Base
  has_many :photos, :dependent => :destroy
  has_many :albums, :dependent => :destroy
  
  attr_accessible :admin, :first_name, :last_name, :password, :username, :email, :password_confirmation, :password_digest, :twitter_user
  
  has_secure_password
  
  validates_presence_of :first_name, :last_name, :username
  
  validates :username, :format => {:with => /[a-zA-Z][a-zA-Z0-9]*/}
  
  validates_uniqueness_of :username
  
  validates_confirmation_of :password, :message => "ya po, escribe bien la password!"
  
  def complete_name
    first_name + ' ' + last_name
  end
  
end
