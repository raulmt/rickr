class AlbumsPhoto < ActiveRecord::Base
  
  belongs_to :album
  belongs_to :photo

  attr_accessible :photo, :cover, :album_id
end
