class Photo < ActiveRecord::Base
  belongs_to :user
  has_many :albums, :through => :albums_photos
  has_many :albums_photos
  attr_accessible :description, :name, :path, :user, :user_id, :albums_photos_attributes
  
  accepts_nested_attributes_for :albums_photos, :allow_destroy => true, :reject_if => :all_blank
  
  def self.process_file(attrs)
    uploaded_file = attrs[:path]
    if uploaded_file
      File.open(Rails.root.join('public/assets/uploads', attrs[:path].original_filename), 'wb') do |file|
        file.write(uploaded_file.read)
      end
      attrs[:path] = uploaded_file.original_filename
    end
  end
  
  
end
