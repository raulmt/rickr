// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

/* ejemplo de métodos privados, privilegiados y públicos */

function MiClase() {
  function metodoPrivado() {
    alert("privado!");
  }
  
  // atributo privado
  var edad = 0;
  
  // métodos privilegiados, pueden acceder a miembros privados
  this.crecer = function() {
    edad++;
  }
  
  this.getEdad = function() {
    return edad;
  };
  
  this.metodoPrivilegiado = function() {
    // muchas tareas
    metodoPrivado();
  };
}

// miembros públicos, no podrán acceder a miembros privados
MiClase.prototype.MAYORIA_DE_EDAD = 18;
MiClase.prototype.metodoPublico = function() {
  alert("publico!");
};


// patrón módulo básico
var Rickr = Rickr || {};
Rickr.Validation = Rickr.Validation || {};

// forma de crear plugins para jQuery
(function($, undefined) {
  
  $.fn.miPlugin = function() {
    // instrucciones a ejecutar para cada elemento del objeto jQuery al que se aplica
    // this es el objeto jQuery al que se le llama la función
    
    // para mantener la cadena...
    return this;
  };
  
})(jQuery);

$(".varios").miPlugin();


// patrón módulo más elaborado

// recibimos jQuery, el parent module y undefined
var Rickr = (function($, pm, undefined) {
  // creamos un submódulo si es que ya no existe y lo asignamos en "m"
  var m = pm.Validation = pm.Validation || {};
  
  // agregamos los métodos públicos de nuestro submódulo
  m.checkRequired = function() {};
  
  // y también algunas funciones privadas para usarlas dentro de las públicas
  function f1() {
    
  }
  
  // también podemos agregarle funciones a nuestro parent module
  pm.publica = function() {
    f1();
  };
  
  return pm;
})(jQuery, Rickr || {});
// entregamos jQuery, el módulo o un objeto vacío si es que aún no existe y.. nada más.

Rickr.publica
Rickr.Validation.checkRequired