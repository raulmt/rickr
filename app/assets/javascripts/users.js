// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var Rickr = Rickr || {};
Rickr.Validation = Rickr.Validation || {};

Rickr.Validation.checkRequired = function(field) {
  var value = field.val();
  if (!value || value.trim().length == 0) {
    if (!field.data("error")) {
      field.after('<span class="errorMessage">debes ingresar este campo</span>');      
    }
    return true;
  }
  return false;
}




var first_name_error = false;
$(
  function() {
    var $form = $('#new_user');
    if ($form.length > 0) {
      
      $form.on('submit', function(e) {
        var first_name_field = $('#user_first_name');
        var v = Rickr.Validation;
        var error = v.checkRequired.checkRequired(first_name_field);
        if (error) {
          first_name_field.data("error", true);
        } else {
          first_name_field.data("error", false);
          // eliminar el mensaje de error
        }
        error = error || checkRequired($('#user_last_name'));
        if (error) {
          e.preventDefault();
        }
      });
      
    }
  }
);