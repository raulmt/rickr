// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_self
//= require_tree .

$(
  function() {
    $('a[data-type=html]').on('ajax:before', function() {
      console.log('Before');
      $('#loading').show();
    });
    $('a[data-type=html]').on('ajax:success', function(event, data) {
      var $that = $(this);
      var elem = $that.data('update-element');
      var $elem = $('#' + elem);
      if ($elem.length > 0) {
        $elem.html(data);
      } else {
        $(this).after(data);
      }
      console.log('success');
      
    });
    $('a[data-type=html]').on('ajax:failure', function() {
      console.log('failure');
    });
    $('a[data-type=html]').on('ajax:complete', function() {
      console.log('complete');
      $('#loading').hide();
      
    });
  }
);